// $Id

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Known limitations
 

INTRODUCTION
------------

Authors:
* David N�grier (moufmouf)

With the Static HTML Viewer module, you can bind a static HTML web site directly 
into Drupal.

This is very useful if you want to expose a set of HTML pages into your website
without having to create a node for each page. For instance, if you are
a developer and generate documentation for your code (using Doxygen in PHP
or Javadoc in Java), you will end up with a lot of HTML pages. You might want
to expose those pages inside your Drupal website, in order to benefit from 
all the benefits of a Drupal install (themes, etc...) This module will
help you do this: the content of every HTML page is copied in the content
of the theme.


INSTALLATION
------------

1. Copy the static_html_viewer directory to your sites/SITENAME/modules directory
   or sites/all/modules directory.

2. Enable the module at Administer >> Site building >> Modules.

3. Once the module is installed, a new content type for Static Web Sites is 
   available. Create a new static website at Create content >> Static Html Viewer.
   
4. Give a title, the path to the website on your server, and a web path.

5. That's it. :)

For instance, let's say your generated website is in /var/doc/myproject:

 - /var/doc/myproject/index.html
 - /var/doc/myproject/page1.html
 - /var/doc/myproject/page2.html

To access this site, you can use:

Path to web site: /var/doc/myproject
Path: /myproject

Once the Static Web Site node is created, you can access the pages at:
 - http://localhost/[your drupal install]/myproject/index.html
 - http://localhost/[your drupal install]/myproject/page1.html
 - http://localhost/[your drupal install]/myproject/page2.html


KNOWN LIMITATIONS
-----------------

The module requires all HTML pages to end up with the .html extension.
PHP files will be ignored (you will get a HTTP 404 error if you try to access a PHP file)
All other files are passed "as-is" to the browser.

The module will extract the content of the <body> tag. Everything in <head> 
(scripts, stylesheets, etc...) is not loaded.
